# CMake generated Testfile for 
# Source directory: /home/banner/thor/src/rosserial/rosserial_client
# Build directory: /home/banner/thor/build/rosserial/rosserial_client
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_rosserial_client_gtest_float64_test "/home/banner/thor/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/banner/thor/build/test_results/rosserial_client/gtest-float64_test.xml" "--return-code" "/home/banner/thor/devel/lib/rosserial_client/float64_test --gtest_output=xml:/home/banner/thor/build/test_results/rosserial_client/gtest-float64_test.xml")
add_test(_ctest_rosserial_client_gtest_subscriber_test "/home/banner/thor/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/banner/thor/build/test_results/rosserial_client/gtest-subscriber_test.xml" "--return-code" "/home/banner/thor/devel/lib/rosserial_client/subscriber_test --gtest_output=xml:/home/banner/thor/build/test_results/rosserial_client/gtest-subscriber_test.xml")
