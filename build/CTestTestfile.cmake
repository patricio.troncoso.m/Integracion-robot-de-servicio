# CMake generated Testfile for 
# Source directory: /home/banner/thor/src
# Build directory: /home/banner/thor/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(audio_common/audio_common)
subdirs(audio_common/audio_common_msgs)
subdirs(pocketsphinx)
subdirs(rosserial/rosserial)
subdirs(rosserial/rosserial_arduino)
subdirs(rosserial/rosserial_mbed)
subdirs(rosserial/rosserial_msgs)
subdirs(rosserial/rosserial_python)
subdirs(rosserial/rosserial_tivac)
subdirs(rosserial/rosserial_xbee)
subdirs(rosserial/rosserial_client)
subdirs(audio_common/audio_capture)
subdirs(audio_common/audio_play)
subdirs(ros_aiml)
subdirs(rosserial/rosserial_server)
subdirs(audio_common/sound_play)
subdirs(rosserial/rosserial_embeddedlinux)
subdirs(rosserial/rosserial_test)
subdirs(rosserial/rosserial_windows)
subdirs(post/post)
subdirs(randtur/rturt)
subdirs(hello)
subdirs(tutorialpy)
