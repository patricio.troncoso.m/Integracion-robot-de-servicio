set(_CATKIN_CURRENT_PACKAGE "tutorialpy")
set(tutorialpy_VERSION "0.0.0")
set(tutorialpy_MAINTAINER "banner <banner@todo.todo>")
set(tutorialpy_PACKAGE_FORMAT "2")
set(tutorialpy_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(tutorialpy_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "std_msgs")
set(tutorialpy_BUILDTOOL_DEPENDS "catkin")
set(tutorialpy_BUILDTOOL_EXPORT_DEPENDS )
set(tutorialpy_EXEC_DEPENDS "roscpp" "rospy" "std_msgs")
set(tutorialpy_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(tutorialpy_TEST_DEPENDS )
set(tutorialpy_DOC_DEPENDS )
set(tutorialpy_URL_WEBSITE "")
set(tutorialpy_URL_BUGTRACKER "")
set(tutorialpy_URL_REPOSITORY "")
set(tutorialpy_DEPRECATED "")