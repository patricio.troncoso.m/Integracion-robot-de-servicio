
"use strict";

let SoundRequestActionResult = require('./SoundRequestActionResult.js');
let SoundRequestActionGoal = require('./SoundRequestActionGoal.js');
let SoundRequestActionFeedback = require('./SoundRequestActionFeedback.js');
let SoundRequestFeedback = require('./SoundRequestFeedback.js');
let SoundRequestGoal = require('./SoundRequestGoal.js');
let SoundRequestResult = require('./SoundRequestResult.js');
let SoundRequestAction = require('./SoundRequestAction.js');
let SoundRequest = require('./SoundRequest.js');

module.exports = {
  SoundRequestActionResult: SoundRequestActionResult,
  SoundRequestActionGoal: SoundRequestActionGoal,
  SoundRequestActionFeedback: SoundRequestActionFeedback,
  SoundRequestFeedback: SoundRequestFeedback,
  SoundRequestGoal: SoundRequestGoal,
  SoundRequestResult: SoundRequestResult,
  SoundRequestAction: SoundRequestAction,
  SoundRequest: SoundRequest,
};
