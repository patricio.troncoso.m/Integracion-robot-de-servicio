import speech_recognition as sr

r = sr.Recognizer()
m = sr.Microphone()

try:
    print("A moment of silence, please...")
    with m as source: r.adjust_for_ambient_noise(source)
    print("Set minimum energy threshold to {}".format(r.energy_threshold))
    while True:
        print("Di algo!")
        with m as source: audio = r.listen(source)
        print("Espera...")
        try:
            # recognize speech using Google Speech Recognition
            value = r.recognize_google(audio, language='es')
            texto=str(value)
            if 'enciende un LED' in value:
                print('led encendido')
            if 'hora' in value:
                print('ES LA HORA DE TU PAPA')    

            # we need some special handling here to correctly print unicode characters to standard output
            if str is bytes:  # this version of Python uses bytes for strings (Python 2)
                print(u"escuché {}".format(value).encode("utf-8"))
            else:  # this version of Python uses unicode for strings (Python 3+)
                print("escuché {}".format(value))
        except sr.UnknownValueError:
            print("Oops! no te entendí")
        except sr.RequestError as e:
            print("Problemas de conexion con el servirdor; {0}".format(e))
except KeyboardInterrupt:
    pass
