#!/usr/bin/env python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- coding: 850 -*-
import os, sys
# Simple talker demo that published std_msgs/Strings messages
# to the 'chatter' topic
import unicodedata
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64
import speech_recognition as sr
import pyttsx as tts

engine = tts.init()
engine.setProperty('rate',200)                 #para ajustar velocidad al hablar    
engine.setProperty('voice',b'spanish')




r = sr.Recognizer()
m = sr.Microphone()

def talker():

    pub = rospy.Publisher('recognizer/output', String, queue_size=10)
    ard = rospy.Publisher('caracteres', Float64, queue_size=10)
    rospy.init_node('talker')
    print("A moment of silence, please...")
    with m as source: r.adjust_for_ambient_noise(source)
    print("Set minimum energy threshold to {}".format(r.energy_threshold))
    while not rospy.is_shutdown():
         print("Di algo!")
         with m as source: audio = r.listen(source)
         print("Espera...")
         try:
            # recognize speech using Google Speech Recognition
            value = r.recognize_google(audio, language='es')
            print(u"dijiste %s" %value)
            pub.publish(value)
            if 'enciende un LED' in value:
                print('No Hay problema LED ON !!!')
                engine.say('No Hay problema LED ON')
                engine.runAndWait()
                x= 1.0
                ard.publish(x)
            if 'gracias' in value:
                print('De Nada... LED OFF !!!')
                engine.say('De Nada... LED OFF')
                engine.runAndWait() 
                x= 2.0
                ard.publish(x)
            
            #if 'hora' in value:
             #   print('ES LA HORA DE TU PAPA')    
         except sr.UnknownValueError:
            print("Oops no te entendi")
         except sr.RequestError as e:
            print("Problemas de conexion con el servirdor; {0}".format(e))
            pass

	



if __name__ == '__main__':
    try:
        talker()
        rospy.loginfo(msg)
        
    except rospy.ROSInterruptException:
        pass
