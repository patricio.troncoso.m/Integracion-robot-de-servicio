// This program publishes randomly − generated velocity
// messages for turtlesim.
#include <ros/ros.h>
#include <geometry_msgs/Twist.h> // For geometry_msgs::Twist <nombre paquete/tipo mensaje>
#include <stdlib.h> // For rand () and RAND_MAX

int main(int argc, char **argv)
{
// Initialize the ROS system and become a node .
ros::init(argc,argv,"Publicador_velocidad");
ros::NodeHandle nh;
// Create apublisherobject.
ros::Publisher pub = nh.advertise<geometry_msgs::Twist >("turtle1/cmd_vel", 1000);//objeto publicador
//ros::Publisher pub = node handle.advertise<tipo mensaje>("nombre topico", queue size); tipomensaje=clase definida en cabecera
// Seed the random number generator .
srand(time( 0 ) ) ;
// Loop at 2Hz until the node i s shut down.
ros::Rate rate( 2 ) ;
while (ros::ok ())
{
// Create and fill in the message . The other four
// fields, which are ignored by turtlesim , default to 0.
geometry_msgs::Twist msg;//define la clase

msg.linear.x= double(rand())/double (RAND_MAX);
msg.angular.z=2*double(rand ())/double (RAND_MAX) -1;
// Publish the message .
pub.publish(msg);

// Send a message to rosout with the d e t a i l s .
ROS_INFO_STREAM("Enviando _comandos_ velocidad_aleatoria : "
<< "_linear=" <<msg.linear.x<< "_angular= " << msg.angular.z);

// Wait u n t i l i t ' s time f o r another i t e r a t i o n .
rate.sleep() ;
}

}
