//programa para determinar la pocicion de la tortuga en la pantalla de turtlesim PAG56
#include <ros/ros.h>
#include <turtlesim/Pose.h> //agrega el tipo de mensaje en el encabezado
#include <iomanip> //para std::setprecission y std::fixed
void poseMessageReceived(const turtlesim::Pose &msg) //inicio callback se ejecuta cada vez que lega un mensaje de posicion nueva
// turtlesim::Pose=>tipo de mensaje a recibir,requiere de encabezado
//void funcion_nombre(const pkg_name::type_name &msg
{
    ROS_INFO_STREAM(std::setprecision(2)<< std::fixed << "posicion= ("<< msg.x<<" , "<< msg.y <<")"<<"_direccion= "<<msg.theta);
 //si la variacion de el estado del puntero de posicion es mayor a la precision, se publica la nueva posición.
 }

int main(int argc,char **argv)
{
    ros::init(argc, argv, "Suscribe_y_publica_Pose");   //inicializa ros y lo coniverte en un nodo
    ros::NodeHandle nh;

    ros::Subscriber sub= nh.subscribe("turtle1/pose",1000, &poseMessageReceived); //crea una subscripcion a turtle1/pose con queue=1000 y lo guarda en la direccionde poseMessageReceived
    //ros::Suscriber sub= node_handle.suscribe("topic_name",queue_size, &pointer_to_callback_function)

    ros::spin();    //deja el control a ROS spin() permite repetir el proceso hasta que ros o el usuario lo detenga spinOnce(), realiza la tarea solo una vez

}
